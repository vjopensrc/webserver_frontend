"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var TicketService = (function () {
    function TicketService(http) {
        this.http = http;
        this.headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        this.ticketsurl = 'http://localhost:8080/tickets';
        this.notesurl = 'http://localhost:8080/notes';
        this.editticketsurl = 'http://localhost:8080/edittickets';
    }
    TicketService.prototype.getTickets = function () {
        return this.http.get(this.ticketsurl, { headers: this.headers })
            .map(function (response) { return response.json(); });
    };
    TicketService.prototype.getTicket = function (id) {
        var url = this.ticketsurl + "/" + id;
        return this.http
            .get(url, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    TicketService.prototype.create = function (ticket) {
        console.log("ss");
        return this.http
            .post(this.ticketsurl, JSON.stringify(ticket), { headers: this.headers })
            .map(function (res) { return res.json().Outcome[0]; });
    };
    TicketService.prototype.createNote = function (note) {
        return this.http
            .post(this.notesurl, JSON.stringify(note), { headers: this.headers })
            .map(function (res) { return res.json().Outcome[0]; });
    };
    TicketService.prototype.edit = function (ticket) {
        console.log("ss");
        return this.http
            .post(this.editticketsurl, JSON.stringify(ticket), { headers: this.headers })
            .map(function (res) { return res.json().Outcome[0]; });
    };
    TicketService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], TicketService);
    return TicketService;
}());
exports.TicketService = TicketService;
//# sourceMappingURL=ticket.service.js.map