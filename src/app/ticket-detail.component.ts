
import { Component, Input, OnInit } from '@angular/core';

import { ActivatedRoute, Params }   from '@angular/router';
import { Location }                 from '@angular/common';

import { Ticket } from './ticket';
import { Note } from './note';
import { TicketService } from './ticket.service';

import 'rxjs/add/operator/switchMap';

@Component({
  moduleId: module.id,
  selector: 'my-ticket-detail',
  templateUrl: './ticket-detail.component.html',
})

export class TicketDetailComponent implements OnInit{
    note: Note;
    ticket: Ticket;
    notes: Note[];

    constructor(
      private ticketService: TicketService,
      private route: ActivatedRoute,
      private location: Location
	  ) {}

    ngOnInit(): void {
	  this.route.params
	    .switchMap((params: Params) => this.ticketService.getTicket(+params['id']))
	    .subscribe(result => {
            this.ticket = result.Outcome[0];
            this.notes = result.Outcome[1];
            console.log("notes",this.notes);
        });
	  }

    save(): void{
      this.ticketService.edit(this.ticket)
			  .subscribe(ticket => {
				  
			});
    }

    addnote(ticketid: number,name: string,desc: string): void {
        this.note = new Note;
		    this.note.name = name.trim();
        this.note.description = desc.trim();
        this.note.ticketid = ticketid;
		    this.ticketService.createNote(this.note)
			  .subscribe(note => {
				  this.notes.push(note);
			});
	}
}