"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var common_1 = require('@angular/common');
var note_1 = require('./note');
var ticket_service_1 = require('./ticket.service');
require('rxjs/add/operator/switchMap');
var TicketDetailComponent = (function () {
    function TicketDetailComponent(ticketService, route, location) {
        this.ticketService = ticketService;
        this.route = route;
        this.location = location;
    }
    TicketDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params
            .switchMap(function (params) { return _this.ticketService.getTicket(+params['id']); })
            .subscribe(function (result) {
            _this.ticket = result.Outcome[0];
            _this.notes = result.Outcome[1];
            console.log("notes", _this.notes);
        });
    };
    TicketDetailComponent.prototype.save = function () {
        this.ticketService.edit(this.ticket)
            .subscribe(function (ticket) {
        });
    };
    TicketDetailComponent.prototype.addnote = function (ticketid, name, desc) {
        var _this = this;
        this.note = new note_1.Note;
        this.note.name = name.trim();
        this.note.description = desc.trim();
        this.note.ticketid = ticketid;
        this.ticketService.createNote(this.note)
            .subscribe(function (note) {
            _this.notes.push(note);
        });
    };
    TicketDetailComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'my-ticket-detail',
            templateUrl: './ticket-detail.component.html',
        }), 
        __metadata('design:paramtypes', [ticket_service_1.TicketService, router_1.ActivatedRoute, common_1.Location])
    ], TicketDetailComponent);
    return TicketDetailComponent;
}());
exports.TicketDetailComponent = TicketDetailComponent;
//# sourceMappingURL=ticket-detail.component.js.map