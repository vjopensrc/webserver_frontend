export class Note {
  id: number;
  ticketid: number;
  name: string;
  description: string;
  updated: number;
}
