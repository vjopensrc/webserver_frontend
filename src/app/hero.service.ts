import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Observable }     from 'rxjs/Observable';

import { Hero } from './hero';

@Injectable()
export class HeroService {

  	private headers = new Headers({'Content-Type': 'application/json'});
	private heroesUrl = 'api/heroes111';  // URL to web api

	constructor(private http: Http) { }

	getHeroes(): Observable<Hero[]> { 
    	return this.http.get(this.heroesUrl)
               .map(response => response.json().data as Hero[]);
  	}

	getHero(id: number): Observable<Hero> { alert("hi")
		const url = `${this.heroesUrl}/${id}`;
		return this.http.get(url)
			.map(response => response.json().data as Hero);
	}

	create(name: string): Observable<Hero> {
		return this.http
			.post(this.heroesUrl, JSON.stringify({name: name}), {headers: this.headers})
			.map(res => res.json().data);
	}


	update(hero: Hero): Observable<Hero> {
		const url = `${this.heroesUrl}/${hero.id}`;
		return this.http
			.put(url, JSON.stringify(hero), {headers: this.headers})
			.map(() => hero);
	}


	delete(id: number): Observable<void> {
		const url = `${this.heroesUrl}/${id}`;
		return this.http.delete(url, {headers: this.headers})
			.map(() => null);
	}
}
