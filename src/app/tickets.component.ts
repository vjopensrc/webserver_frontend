import { Component } from '@angular/core';
import { OnInit } from '@angular/core';

import { Ticket } from './ticket';

import { TicketService } from './ticket.service';

@Component({
    moduleId: module.id,
    selector: 'my-tickets',
    templateUrl: './tickets.component.html',
})

export class TicketsComponent implements OnInit {
   
    tickets: Ticket[];
    ticket: Ticket;

    constructor(
        private ticketservice: TicketService
    ){}

    ngOnInit(): void {
		this.getTickets();
	}

    getTickets() {
        this.ticketservice.getTickets().subscribe(
            res => {
                this.tickets = res.Outcome[0];
            },
            error => console.log(error)
        );
    }

    add(subject: string,desc: string): void {
        this.ticket = new Ticket;
		this.ticket.subject = subject.trim();
        this.ticket.description = desc.trim();
		this.ticketservice.create(this.ticket)
			.subscribe(ticket => {
				this.tickets.push(ticket);
			});
	}

}