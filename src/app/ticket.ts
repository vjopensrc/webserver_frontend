export class Ticket {
  id: number;
  subject: string;
  description: string;
  updated: number;
}
