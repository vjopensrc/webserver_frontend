import { Component } from '@angular/core';

@Component({
  selector: 'my-app',
  template: `
   <div style="color:#fff; padding:8px;font-size:24px;background:#8a4dfa;margin:8px;border-radius:2px">{{title}}</div>
   <my-tickets></my-tickets>
 `
})

export class AppComponent {
  title = 'DataSync';
}
