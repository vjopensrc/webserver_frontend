import { Injectable } from '@angular/core';
import { Response, Http,Headers } from '@angular/http';
import { Observable }     from 'rxjs/Observable';

import { Ticket } from './ticket';
import { Note } from './note';

@Injectable()
export class TicketService {
    private headers = new Headers({'Content-Type': 'application/json'});
    private ticketsurl = 'http://localhost:8080/tickets'
    private notesurl = 'http://localhost:8080/notes'
    private editticketsurl = 'http://localhost:8080/edittickets'

    constructor(private http: Http){}

    getTickets():Observable<any>{
        return this.http.get(this.ticketsurl,{headers: this.headers})
               .map(response => response.json());
    }

    getTicket(id: number):Observable<any> {
        const url = `${this.ticketsurl}/${id}`;
        return this.http
			.get(url, {headers: this.headers})
			.map(res => res.json());
    }

    create(ticket: Ticket):Observable<Ticket> { console.log("ss")
        return this.http
			.post(this.ticketsurl, JSON.stringify(ticket), {headers: this.headers})
			.map(res => res.json().Outcome[0]);
    }

    createNote(note: Note):Observable<Note> {
        return this.http
			.post(this.notesurl, JSON.stringify(note), {headers: this.headers})
			.map(res => res.json().Outcome[0]);
    }

    edit(ticket: Ticket):Observable<Ticket> { console.log("ss")
        return this.http
			.post(this.editticketsurl, JSON.stringify(ticket), {headers: this.headers})
			.map(res => res.json().Outcome[0]);
    }
}