import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { HttpModule }    from '@angular/http';

import { AppRoutingModule }     from './app-routing.module';

// Imports for loading & configuring the in-memory web api

import { AppComponent }         from './app.component';
import { DashboardComponent }   from './dashboard.component';
import { TicketDetailComponent }  from './ticket-detail.component';
import { HeroesComponent }      from './heroes.component';
import { HeroSearchComponent }      from './hero-search.component';
import { TicketsComponent }      from './tickets.component';
import { HeroService }          from './hero.service';
import { TicketService }          from './ticket.service';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule
  ],
  declarations: [
    AppComponent,
    DashboardComponent,
    HeroSearchComponent,
    HeroesComponent,
    TicketsComponent,
    TicketDetailComponent
  ],
  providers: [ 
    HeroService,
    TicketService
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
