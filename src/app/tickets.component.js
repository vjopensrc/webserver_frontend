"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var ticket_1 = require('./ticket');
var ticket_service_1 = require('./ticket.service');
var TicketsComponent = (function () {
    function TicketsComponent(ticketservice) {
        this.ticketservice = ticketservice;
    }
    TicketsComponent.prototype.ngOnInit = function () {
        this.getTickets();
    };
    TicketsComponent.prototype.getTickets = function () {
        var _this = this;
        this.ticketservice.getTickets().subscribe(function (res) {
            _this.tickets = res.Outcome[0];
        }, function (error) { return console.log(error); });
    };
    TicketsComponent.prototype.add = function (subject, desc) {
        var _this = this;
        this.ticket = new ticket_1.Ticket;
        this.ticket.subject = subject.trim();
        this.ticket.description = desc.trim();
        this.ticketservice.create(this.ticket)
            .subscribe(function (ticket) {
            _this.tickets.push(ticket);
        });
    };
    TicketsComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'my-tickets',
            templateUrl: './tickets.component.html',
        }), 
        __metadata('design:paramtypes', [ticket_service_1.TicketService])
    ], TicketsComponent);
    return TicketsComponent;
}());
exports.TicketsComponent = TicketsComponent;
//# sourceMappingURL=tickets.component.js.map